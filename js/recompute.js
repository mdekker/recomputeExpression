/*
 * JavaScript functions for recomputeExpression Plugin for LimeSurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2013 Denis Chenu <http://sondages.pro>
 * @copyright 2013 Practice Lab <https://www.practicelab.com/>
 * @license GPL v3
 * @version 1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
 
 $(function() {
    if(typeof recomputeVar!='undefined'){
    addUpdateResponse();
    }
});

function addUpdateResponse()
{
    var docUrl=document.URL;
    var jsonUrl=recomputeVar.jsonurl;
    var aUrl=docUrl.split('/');
    var surveyid=aUrl[aUrl.indexOf("surveyid")+1];
    if(aUrl.indexOf("admin")>0){
        var controller=aUrl[aUrl.indexOf("admin")+1];
    }else{
        var controller=aUrl[aUrl.indexOf("?r=admin")+1];
    }

    if($('table.detailbrowsetable').length>0)// Browse one response
    {
        // Find the response id
        var responseId=aUrl[aUrl.indexOf("id")+1];
        // OR var responseId= aUrl.pop();
        $('.menubar').eq(1).find('.menubar-main').find(".menubar-left:last").append("<a class='btn btn-small updateanswer' data-responseid='"+responseId+"'><i class='icon-refresh'></i>Update This Answer</a>");
        $('.updateanswer').click(function(){

            $("#updatedsrid").remove();
                $.ajax({
                url: jsonUrl,
                dataType : 'json',
                data : {sid: surveyid, srid: responseId},
                success: function(data){
                    var $dialog = $('<div id="updatedsrid"></div>')
                        .html("<p>"+data.message+"</p>")
                        .dialog({
                            title: data.status,
                            dialogClass: 'updatedsrid',
                            buttons: { 
                                "Ok": function() { $(this).dialog("close"); },
                                "Reload": function() { window.location.reload(); } 
                                },
                            modal: true,
                            close: function () {
                                $(this).remove();
                            }
                        });
                },
                error: function(){
                    var $dialog = $('<div id="updatedsrid"></div>')
                        .html("<p>An error was occured</p>")
                        .dialog({
                            title: "Error",
                            dialogClass: 'updatedsrid',
                            buttons: { 
                                "Ok": function() { $(this).dialog("close"); },
                                },
                            modal: true,
                            close: function () {
                                $(this).remove();
                            }
                        });
                },
            });
        });
    }
    if(controller=='responses'){
        $('.menubar').eq(0).find('.menubar-main').find(".menubar-left:last").append("<a class='btn btn-small updateanswers'><i class='icon-refresh'></i>Update all submitted answers</a>");
            $(".updateanswers").click(function(){
                var jsonurl=$(this).attr('rel');
                  $("#updatedsrid").remove();
                  var $dialog = $('<div id="updatedsrid" style="overflow-y:scroll"></div>')
                    .html("")
                    .dialog({
                      height: 200,
                      title: "Status",
                      dialogClass: 'updatedsrid',
                      buttons: { Cancel: function() { $(this).dialog("close"); } },
                      modal: true,
                      close: function () {
                          $(this).remove();
                      }
                  });
                loopUpdateResponse(jsonUrl,surveyid,0);
            });
    }
}

/*
* Used to update response one by one
* @param jsonurl : The json Url to request
* @param {integer} surveyid : The survey id
* @param {integer} responseid : The response id
*/
function loopUpdateResponse(jsonurl,surveyid,responseid) {
  if($("#updatedsrid").length>0)
  {
    $.ajax({
        url: jsonurl,
        dataType : 'json',
        data : {sid: surveyid, srid: responseid,next : 1},
        success: function (data) {
          $("#updatedsrid").prepend("<p style='margin:0;display:none'>"+data.message+"</p>");
          $("#updatedsrid p:first-child").slideDown(500);
          //$("#updatedsrid p:nth-child(6)").fadeOut(500,function() {$(this).remove();});
            if (data.next) {
                loopUpdateResponse(jsonurl,surveyid,data.next);
            } else {
              $("#updatedsrid").closest(".ui-dialog").find(" .ui-dialog-buttonset .ui-button-text").html("Done");
              $("#updatedsrid").prepend("<p style='margin:0;font-weight:700'>Done</p>");
            }
        },
        error: function(){
          $("#updatedsrid").prepend("<p style='margin:0;display:none'>An error was occured</p>");
          $("#updatedsrid p:first-child").slideDown(500);
          $("#updatedsrid").closest(".ui-dialog").find(" .ui-dialog-buttonset button").html("Done");
          $("#updatedsrid").prepend("<p style='margin:0;font-weight:700'>Done</p>");
        }
    });
  }
}
